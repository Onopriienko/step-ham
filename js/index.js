// Первые табы
let tabs = document.querySelectorAll('.button-options');
tabs.forEach(function(trigger) {
    trigger.addEventListener('click', function() {
        let id = this.getAttribute('data-tab'),
            content = document.querySelector('.tab-content[data-tab="'+id+'"]'),
            activeTab = document.querySelector('.button-options.active'),
            activeContent = document.querySelector('.tab-content.active');
        activeTab.classList.remove('active');
        trigger.classList.add('active');
        activeContent.classList.remove('active');
        content.classList.add('active');
    });
});

//Вторые Табы

let fActive = '';

function filterTabs(theme){
    if(fActive !== theme){
        $('.sort').filter('.'+theme).fadeIn();
        $('.sort').filter(':not(.'+theme+')').fadeOut();
        fActive = theme;
    }
}

$('.for-all').click(function(){ filterTabs('sort'); });
$('.for-graphic').click(function(){ filterTabs('graphic-design'); });
$('.for-design').click(function(){ filterTabs('web-design'); });
$('.for-landing').click(function(){ filterTabs('landing-pages'); });
$('.for-wordpress').click(function(){ filterTabs('wordpress'); });

//Добваить 12 фоток
$('.load-button').click(function () {
    $('.tabs-content-second').removeClass('sort24');
    $(this).remove();
});

//Карусель
$('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.carousel-container'
});
$('.carousel-container').slick({
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    focusOnSelect: true,
    variableWidth: true,
    draggable: true,
    asNavFor: '.slider',
    prevArrow: '<button class="prev-button"><</button>',
    nextArrow:'<button class="next-button">></button>',
    cssEase: 'cubic-bezier(0.600, -0.280, 0.735, 0.045)',
    speed: 900
});

